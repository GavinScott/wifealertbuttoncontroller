import boto3
import json
import sys
import time
import RPi.GPIO as GPIO
import logging
import os
import datetime


def get_path_in_cwd(fname):
    return '/'.join(os.path.abspath(__file__).split('/')[:-1]) + f'/{fname}'


print('Setting up logger')
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%y-%m-%d %H:%M:%S',
    filename=get_path_in_cwd('button-controller-log.log'),
    filemode='w')
logging.info('Time: ' + datetime.datetime.now().isoformat())

console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(logging.Formatter(
    '%(name)-12s: %(levelname)-8s %(message)s'))
logging.getLogger().addHandler(console)

OK_STATE = 'OK'
ALARM_STATE = 'ALARM'
PARAM_NAME = 'WifeAlertParam'
BUTTON_PIN = 10
GREEN_LED_PIN = 8
RED_LED_PIN = 16


class WifeAlertButton:
    def __init__(self):
        logging.info(f'Initializing at {datetime.datetime.utcnow()}')

        credsFname = get_path_in_cwd('creds.json')
        logging.info(f'Loading credentials from file: {credsFname}')
        with open(credsFname, 'r') as f:
            self.creds = json.load(f)

        logging.info('Creating boto3 session')
        session = boto3.Session(
            aws_access_key_id=self.creds['aws_access_key_id'],
            aws_secret_access_key=self.creds['aws_secret_access_key'],
            region_name=self.creds['region'])

        logging.info('Creating SSM client')
        self.ssm = session.client('ssm')
        self.sns = session.client('sns')

        logging.info('Getting current AWS account id')
        self.accountId = session.client(
            'sts').get_caller_identity().get('Account')
        logging.info(f'AWS account id: {self.accountId}')

        logging.info('Setting up GPIO')
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        try:
            GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            for pin in [GREEN_LED_PIN, RED_LED_PIN]:
                GPIO.setup(pin, GPIO.OUT, initial=GPIO.LOW)
        except (KeyboardInterrupt, SystemExit):
            print()
            logging.info('Terminating')
            self.cleanup()
            exit(0)
        except Exception as ex:
            print('Something went wrong!')
            logging.error('Fatal error!', exec_info=True)
            self.cleanup()
            exit(-1)

        self.state = None
        self.run_startup_light_sequence()
        self.sync_state()
        self.wait_for_alarm_to_end()
        logging.info('Finished initializing')

    def run_light_sequence(self, sequence):
        delay = 0.2
        leds = [GREEN_LED_PIN, RED_LED_PIN]
        for step in sequence:
            for led in leds:
                if led in step:
                    self.turn_on_light(led)
                else:
                    self.turn_off_light(led)
            time.sleep(delay)
            for led in leds:
                self.turn_off_light(led)
            time.sleep(delay / 2)

    def run_startup_light_sequence(self):
        logging.info('Running startup light sequence')
        self.run_light_sequence([
            [GREEN_LED_PIN],
            [RED_LED_PIN],
            [GREEN_LED_PIN],
            [RED_LED_PIN],
            [GREEN_LED_PIN, RED_LED_PIN],
            [GREEN_LED_PIN, RED_LED_PIN],
            [GREEN_LED_PIN, RED_LED_PIN]
        ])

    def cleanup(self):
        logging.info('Running shutdown light sequence')
        self.run_light_sequence([
            [GREEN_LED_PIN, RED_LED_PIN],
            [GREEN_LED_PIN, RED_LED_PIN],
            [GREEN_LED_PIN, RED_LED_PIN]
        ])
        logging.info('Cleaning up GPIO')
        GPIO.cleanup()

    def turn_on_light(self, led):
        GPIO.output(led, GPIO.HIGH)

    def turn_off_light(self, led):
        GPIO.output(led, GPIO.LOW)

    def set_state(self, value):
        if self.state == value:
            return
        logging.info(f'Detected state change to {value}')
        self.state = value
        if self.state == OK_STATE:
            logging.info('Turning green light on and red light off')
            self.turn_on_light(GREEN_LED_PIN)
            self.turn_off_light(RED_LED_PIN)
        else:
            logging.info('Turning red light on and green light off')
            self.turn_on_light(RED_LED_PIN)
            self.turn_off_light(GREEN_LED_PIN)

    def sync_state(self):
        logging.info('Syncing alarm state')
        newState = self.ssm.get_parameter(Name=PARAM_NAME)[
            'Parameter']['Value']
        logging.info(f'State: {newState}')
        self.set_state(newState)

    def wait_for_alarm_to_end(self):
        logging.info(f'Waiting for state to not be {ALARM_STATE}')
        while self.state == ALARM_STATE:
            logging.info('Checking parameter')
            self.sync_state()
            if self.state == ALARM_STATE:
                logging.info('Waiting to poll again')
                time.sleep(20)
        logging.info(f'Alarm is {OK_STATE}, no longer waiting')

    def trigger_alarm(self, channel=None):
        logging.info('***** WIFE PUSHED THE BUTTON *****')
        if self.state == OK_STATE:
            logging.info('Triggering alarm')
            self.set_state(ALARM_STATE)
            logging.info('Publishing SNS message')
            self.sns.publish(
                Message=json.dumps({'alarm': ALARM_STATE}),
                TopicArn=f'arn:aws:sns:{self.creds["region"]}:{self.accountId}:WifeAlertChangeEventTopic',
                MessageAttributes={
                    'WifeAlert': {
                        'DataType': 'String',
                        'StringValue': ALARM_STATE
                    }
                }
            )
            # wait because the parameter won't be set immediately
            time.sleep(45)
            self.wait_for_alarm_to_end()
        else:
            logging.info('Already in alarm state, doing nothing')

    def run(self):
        try:
            while True:
                if GPIO.input(BUTTON_PIN) and self.state == OK_STATE:
                    print()
                    self.trigger_alarm()
        except (KeyboardInterrupt, SystemExit):
            print()
            logging.info('Terminating')
            self.cleanup()
            exit(0)
        except Exception as ex:
            print('Something went wrong!')
            logging.error('Fatal error!', exec_info=True)
            self.cleanup()
            exit(-1)


if __name__ == '__main__':
    WifeAlertButton().run()
